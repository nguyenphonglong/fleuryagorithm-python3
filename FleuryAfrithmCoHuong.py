import numpy as np
class Graph():
    global __currentVertice, __currentEdge
    global __visited

    def __init__(self,file):
        (self.numV,self.numE,self.edge)= self.readDataFromFile(file)
        self.__visited = np.ones((1, self.numV)).flatten()
        self.__currentEdge = self.edge
        self.__currentVertice = np.ones((1, self.numV)).flatten()

    def readDataFromFile(self,filename):
        file = open(filename)
        listIsExist = file.read().splitlines()
        numV = int(listIsExist[0])
        edge = np.zeros((numV, numV))
        for i in range(numV):
            a = listIsExist[i + 1].split(" ")
            for j in range(numV):
                edge[i][j] = int(a[j])
        numE = 0
        for i in range(numV):
            for j in range(numV):
                numE += edge[i][j]
        numE=int(numE)
        return (numV, numE,edge)

    def checkConditionFleury(self,u):
        listDeguOut = []
        listDeguInto=[]
        for i in range(self.numV):
            listDeguOut.append(sum(self.edge[i]))
        for i in range(self.numV):
            degInto=0
            for j in range(self.numV):
                degInto+=self.edge[i][j]
            listDeguInto.append(degInto)
        conditionAllIntoDegEqualOutDeg = False
        count = 0
        V = np.ones((1, self.numV)).flatten()
        for i in range(self.numV):
            if listDeguInto[i]==listDeguOut[i]:
                count += 1
        if (count == self.numV):
            conditionAllIntoDegEqualOutDeg = True
        if (conditionAllIntoDegEqualOutDeg and self.checkConnectedStrong(u, V, self.edge)):
            return True
        else:
            return False


    def DFSStrong(self,x,curentVertices,curentEdges):
        self.__visited[x]=0
        for v in range(self.numV):
            if (self.__visited[v] == 1 and curentEdges[x][v] == 1 ):
                self.__visited[v] = 0
                self.DFSWeak(v, curentVertices, curentEdges)

    def DFSWeak(self,x,curentVertices,curentEdges):
        self.__visited[x]=0
        for v in range(self.numV):
            if (self.__visited[v] == 1 and (curentEdges[x][v] == 1 or curentEdges[v][x] == 1)):
                self.__visited[v] = 0
                self.DFSWeak(v, curentVertices, curentEdges)

    def checkConnectedStrong(self,x, curentVertices, curentEdges):
        for i in range(self.numV):
            self.__visited[i] = curentVertices[i]

        self.DFSStrong(x, curentVertices, curentEdges)
        sum = 0
        for i in range(self.numV):
            sum += self.__visited[i]
        if (sum == 0):
            return True
        else:
            return False

    def checkConnectedWeak(self,x, curentVertices, curentEdges):
        for i in range(self.numV):
            self.__visited[i] = curentVertices[i]

        self.DFSWeak(x, curentVertices, curentEdges)
        sum = 0
        for i in range(self.numV):
            sum += self.__visited[i]
        if (sum == 0):
            return True
        else:
            return False

    def FleuryAgorithm(self, u):
        if self.checkConditionFleury(u):
            print("Thỏa mãn điều kiện Fleury cho đồ thị có hướng là đồ thị có hướng liên thông mạnh và tất cả các đỉnh có bán bậc vào bằng bán bậc ra")
            print("Đường đi Euler tìm được:\n")
            for count in range(self.numE):
                currentDeguOut = 0
                currentDeguInto=0

                for j in range(self.numV):
                    currentDeguOut += self.__currentEdge[u][j]
                    currentDeguInto +=self.__currentEdge[j][u]
                if (currentDeguOut == 1):
                    if currentDeguInto==0:
                        self.__currentVertice[u] = 0
                    for j in range(self.numV):
                        if (self.__currentEdge[u][j] == 1):
                            self.__currentEdge[u][j] = 0
                            print("Bước ", count + 1, ": ", u, "-", j)
                            u = j
                            break
                else:
                    for j in range(self.numV):
                        if (self.__currentEdge[u][j] == 1 and self.__currentVertice[j] == 1):
                            self.__currentEdge[u][j] = 0
                            if (self.checkConnectedWeak(u, self.__currentVertice,self.__currentEdge)):
                                print("Bước ", count + 1, ": ", u, "-", j)
                                u = j
                                break
                            else:
                                self.__currentEdge[u][j] = 1

        else:
            print("Đồ thị đã cho không thỏa mã điều kiện Fleury nên không có chu trình Euler")

a=Graph("input.txt")
print("Số đỉnh của đồ thị là ",a.numV)
print("Số cạnh của đồ thị là ",a.numE)
x=int(input("Nhập đỉnh bạn muốn bắt đầu chu trình Euler: "))
a.FleuryAgorithm(x)