import numpy as np
class Graph():
    global __currentVertice, __currentEdge
    global __visited

    def __init__(self,file):
        (self.numV,self.numE,self.edge)= self.readDataFromFile(file)
        self.__visited = np.ones((1, self.numV)).flatten()
        self.__currentEdge = self.edge
        self.__currentVertice = np.ones((1, self.numV)).flatten()

    def readDataFromFile(self,filename):
        file = open(filename)
        listIsExist = file.read().splitlines()
        numV = int(listIsExist[0])
        edge = np.zeros((numV, numV))
        for i in range(numV):
            a = listIsExist[i + 1].split(" ")
            for j in range(numV):
                edge[i][j] = int(a[j])
        numE = 0
        for i in range(numV):
            for j in range(numV):
                numE += edge[i][j]
        numE = int(numE / 2)
        return (numV, numE,edge)

    def checkConditionFleury(self,u):
        listDegu = []
        for i in range(self.numV):
            listDegu.append(sum(self.edge[i]))
        conditionAllEvenVertice = False
        count = 0
        V = np.ones((1, self.numV)).flatten()
        for i in range(self.numV):
            if listDegu[i] % 2 == 0:
                count += 1
        if (count == self.numV):
            conditionAllEvenVertice = True
        if (conditionAllEvenVertice and self.checkConnectedWeak(u, V, self.edge)):
            return True
        else:
            return False


    def DFSWeakWeak(self,x,curentVertices,curentEdges):
        self.__visited[x]=0
        for v in range(self.numV):
            if (self.__visited[v] == 1 and curentEdges[x][v] == 1):
                self.__visited[v] = 0
                self.DFSWeak(v, curentVertices, curentEdges)

    def checkConnectedWeak(self,x, curentVertices, curentEdges):
        for i in range(self.numV):
            self.__visited[i] = curentVertices[i]

        self.DFSWeak(x, curentVertices, curentEdges)
        sum = 0
        for i in range(self.numV):
            sum += self.__visited[i]
        if (sum == 0):
            return True
        else:
            return False

    def FleuryAgorithm(self, u):
        if self.checkConditionFleury(u):
            print("Thỏa mãn điều kiện Fleury đồ thị liên thông và các đỉnh bậc chẵn")
            print("Đường đi Euler tìm được:\n")
            for count in range(self.numE):
                currentDegu = 0
                for j in range(self.numV):
                    currentDegu += self.__currentEdge[u][j]

                if (currentDegu == 1):
                    self.__currentVertice[u] = 0
                    for j in range(self.numV):
                        if (self.__currentEdge[u][j] == 1):
                            self.__currentEdge[u][j] = 0
                            self.__currentEdge[j][u] = 0
                            print("Bước ", count + 1, ": ", u, "-", j)
                            u = j
                            break
                else:
                    for j in range(self.numV):
                        if (self.__currentEdge[u][j] == 1 and self.__currentVertice[j] == 1):
                            self.__currentEdge[u][j] = 0
                            self.__currentEdge[j][u] = 0
                            if (self.checkConnectedWeak(u, self.__currentVertice,self.__currentEdge)):
                                print("Bước ", count + 1, ": ", u, "-", j)
                                u = j
                                break
                            else:
                                self.__currentEdge[u][j] = 1
                                self.__currentEdge[j][u] = 1
        else:
            print("Đồ thị đã cho không thỏa mã điều kiện Fleury nên không có chu trình Euler")

a=Graph("input.txt")
print("Số đỉnh của đồ thị là ",a.numV)
print("Số cạnh của đồ thị là ",a.numE)
x=int(input("Nhập đỉnh bạn muốn bắt đầu chu trình Euler: "))
a.FleuryAgorithm(x)